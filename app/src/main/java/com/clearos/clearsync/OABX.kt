/*
 * OAndBackupX: open-source apps backup and restore app.
 * Copyright (C) 2020  Antonios Hazim
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.clearos.clearsync

import android.content.Context
import com.clearos.dstorage.ApplicationStore
import com.clearos.dstorage.DecentralizedStorageApplication

import timber.log.Timber
import java.io.File

fun Context.getBackupStore(): ApplicationStore {
    return OABX.getInstance(this).getDataStore(getString(R.string.backupStoreName))
}

class OABX : DecentralizedStorageApplication() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        INSTANCE = this

        addRegistrationCallback(packageName) { kca ->
            if (kca != null) {
                Timber.i("Initializing remote API with key client from application.")
            } else {
                Timber.w("Unable to initialize a key-client application. No remote API access.")
            }
            null
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: DecentralizedStorageApplication? = null

        fun getInstance(context: Context): DecentralizedStorageApplication {
            synchronized(this) {
                return INSTANCE!!
            }
        }
    }
}

fun Context.configureBackupStores() {
    val app = OABX.getInstance(this)
    app.createBackupsDirectory()
    app.configureDataStores(getString(R.string.backupFileProviderName), R.xml.file_paths_provider)
}

fun Context.getFullBackupsFolder() : File {
    val result = File(getExternalFilesDir(null), "backups")
    if (!result.exists()) {
        Timber.d("Creating full backups folder at %s", result.absolutePath)
        if (!result.mkdir())
            Timber.w("Unable to create external folder for full backups at %s", result.absolutePath)
    }

    return result
}

private fun DecentralizedStorageApplication.createBackupsDirectory() {
    val target = File(applicationContext.filesDir, "backups")
    if (!target.exists()) if (!target.mkdir()) Timber.w("Unable to auto-create backups directory at $target")
}
