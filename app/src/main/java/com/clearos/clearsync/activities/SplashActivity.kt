/*
 * OAndBackupX: open-source apps backup and restore app.
 * Copyright (C) 2020  Antonios Hazim
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.clearos.clearsync.activities

import android.content.Intent
import android.os.Bundle
import android.os.PowerManager
import com.clearos.clearsync.*
import com.clearos.clearsync.databinding.ActivitySplashBinding
import com.clearos.clearsync.utils.*
import com.clearos.dlt.DidKeys
import com.topjohnwu.superuser.Shell
import io.ipfs.multibase.Base58
import timber.log.Timber

class SplashActivity : BaseActivity() {
    private lateinit var binding: ActivitySplashBinding

    companion object {
        init {
            /*
            * Shell.Config methods shall be called before any shell is created
            * This is the why in this example we call it in a static block
            * The followings are some examples, check Javadoc for more details
            */
            Shell.enableVerboseLogging = BuildConfig.DEBUG
            Shell.setDefaultBuilder(Shell.Builder.create()
                    .setTimeout(20))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setDayNightTheme(getPrivateSharedPrefs().getString(PREFS_THEME, "system"))
        super.onCreate(savedInstanceState)

        Shell.getShell {
            binding = ActivitySplashBinding.inflate(layoutInflater)
            setContentView(binding.root)

//            setUpdateUiElements(binding.progressBar, binding.txtStatusLabel)
//            setRequiredPermissions(arrayOf(
//                    getString(R.string.UpdatePermission),
//                    getString(R.string.KeyDerivationPermission),
//                    Manifest.permission.ACCESS_NETWORK_STATE,
//                    Manifest.permission.CHANGE_NETWORK_STATE
//            ))
//            setForceUpdate(false)
//            setAppRequiresInternet(false)
//            setUpdateSegue { this.setupKeys() }
            setupKeys()
        }
    }

    private fun setupKeys() {
        OABX.getInstance(this).setupKeys({
            if (it == null)
                Timber.w("Application keys could not be setup automatically.")
            else {
                // See if we need to set the encryption password for backups.
                if (getEncryptionPassword() == "") {
                    Timber.i("Setting encryption password for backups using derived key.")
                    val client = OABX.getInstance(this).keyClient
                    val passkeys = client.deriveKeyPair("password", 0)
                    val password = Base58.encode(passkeys.secretKey.asBytes)
                    setEncryptionPassword(password)
                }

                setupStorage(it)
            }

            null
        }, null)
    }

    private fun setupStorage(keys: DidKeys) {
        OABX.getInstance(this).setupStorageClient(packageName
        ) {
            Timber.i("Application DID was derived as ${keys.did}; setting up storage.")
            configureBackupStores()
            segue()

            null
        }
    }

    private fun segue() {
        Timber.d("Segue from splash with derived keys.")
        val prefs = getPrivateSharedPrefs()
        val powerManager = this.getSystemService(POWER_SERVICE) as PowerManager
        val introIntent = Intent(applicationContext, IntroActivityX::class.java)
        if (prefs.getBoolean(PREFS_FIRST_LAUNCH, true)) {
            startActivity(introIntent)
        } else if (hasStoragePermissions &&
                isStorageDirSetAndOk &&
                checkUsageStatsPermission &&
                (prefs.getBoolean(PREFS_IGNORE_BATTERY_OPTIMIZATION, false)
                        || powerManager.isIgnoringBatteryOptimizations(packageName))) {
            introIntent.putExtra(classAddress(".fragmentNumber"), 3)
            startActivity(introIntent)
        } else {
            introIntent.putExtra(classAddress(".fragmentNumber"), 2)
            startActivity(introIntent)
        }
        overridePendingTransition(0, 0)
        finish()
    }
}