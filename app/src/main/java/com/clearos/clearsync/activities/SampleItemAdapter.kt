package com.clearos.clearsync.activities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class SampleItemAdapter(val itemView: Int, val callback: Callback? = null, val size: Int = 10) : RecyclerView.Adapter<SampleItemAdapter.SimpleViewHolder>() {


    inner class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView() {
            itemView.setOnClickListener { callback?.itemClick() }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(itemView, parent, false);
        return SimpleViewHolder(view);
    }

    override fun getItemCount(): Int {
        return size
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.bindView()
    }

    interface Callback {
        fun itemClick()
    }
}