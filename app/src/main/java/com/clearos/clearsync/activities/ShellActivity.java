package com.clearos.clearsync.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.clearos.clearsync.R;
import com.topjohnwu.superuser.Shell;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ShellActivity extends BaseActivity {
    Button btnSubmit;
    EditText txtShellCmd;
    TextView lblOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shell);

        btnSubmit = findViewById(R.id.btnSubmit);
        txtShellCmd = findViewById(R.id.txtShellCmd);
        lblOutput = findViewById(R.id.lblOutput);

        bindEvents();
    }

    private void bindEvents() {
        btnSubmit.setOnClickListener(this::submitCmd);
    }

    private void submitCmd(View v) {
        new Handler().postDelayed(this::submitCmd, 50);
    }

    private String getCommand() {
        return String.format("clearexec %s", txtShellCmd.getText().toString());
    }

    private void submitCmd() {
        try {
            Shell.Result result = Shell.getShell().newJob().add(getCommand()).to(new ArrayList<>(), new ArrayList<>()).exec();
            printOutput(result);
        } catch (Exception e) {
            runOnUiThread(() -> lblOutput.setText(e.getMessage()));
            Timber.d(e, "Error trying to submit shell command");
        }
    }

    @SuppressLint("DefaultLocale")
    private void printOutput(Shell.Result result) {
        List<String> output = new ArrayList<>();
        output.add(String.format("Code: %d", result.getCode()));
        output.add("Output:");
        output.addAll(result.getOut());
        output.add("Error:");
        output.addAll(result.getErr());
        runOnUiThread(() -> lblOutput.setText(String.join("\n", output)));
    }
}